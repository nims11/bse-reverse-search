import json
from flask import Flask, request, render_template
app = Flask(__name__)
import sqlite3

QUERY = """
SELECT shareholding_patterns.name, bhav.name, num_share, percent_share, bhav.value, shareholding_patterns.scrip_id
FROM shareholding_patterns
INNER JOIN (SELECT scrip_id, MAX(qtr_id) as qtr FROM shareholding_patterns group by scrip_id) pp ON pp.qtr = shareholding_patterns.qtr_id and pp.scrip_id = shareholding_patterns.scrip_id
INNER JOIN bhav ON bhav.scrip_id = shareholding_patterns.scrip_id WHERE 
"""

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html')
    elif request.method == 'POST':
        query = request.form['query']
        retRes = []
        add = ""
        foo = []
        for w in query.split():
            if add == "":
                add += "shareholding_patterns.name LIKE ?"
            else:
                add += " AND shareholding_patterns.name LIKE ?"
            foo.append('%'+w+'%')
        conn = sqlite3.connect('bse.db')
        rows = conn.execute(QUERY+add, tuple(foo))
        res = []
        for row in rows:
            res.append({
                'name': row[0],
                'comp.name': row[1],
                'num_share': row[2],
                'num_percent': row[3],
                'value': row[4],
                'scrip_id': row[5]
            })
        return json.dumps(res)


@app.route('/stats', methods=['GET'])
def get_stats():
    conn = sqlite3.connect('bse.db')
    try:
        date = conn.execute("SELECT value FROM stats WHERE key = 'bhav_date'").fetchone()[0]
    except:
        date = 'N/A (Something went wrong, contact Nimesh)'
    return json.dumps(
        {
            'bhav_date': date
        }
    )


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
