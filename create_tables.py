import sys
import sqlite3

def create_daily_rate_table(conn):
    conn.execute('''
CREATE TABLE IF NOT EXISTS bhav
(scrip_id INT PRIMARY KEY NOT NULL,
name VARCHAR(20),
value REAL NOT NULL);
    ''')
    print('Table daily_rate created')


def create_shareholding_patterns_table(conn):
    conn.execute('''
CREATE TABLE IF NOT EXISTS shareholding_patterns
(scrip_id INT NOT NULL,
name VARCHAR(20) NOT NULL,
num_share REAL NOT NULL,
percent_share REAL NOT NULL,
qtr_id REAL NOT NULL,
PRIMARY KEY (scrip_id, name, qtr_id));
    ''')
    print('Table shareholding_patterns created')


def create_stats_table(conn):
    conn.execute('''
CREATE TABLE IF NOT EXISTS stats
(key VARCHAR(20),
value VARCHAR(20));
    ''')
    print('Table stats created')


if __name__ == '__main__':
    conn = sqlite3.connect(sys.argv[1])
    create_daily_rate_table(conn)
    create_shareholding_patterns_table(conn)
    create_stats_table(conn)
