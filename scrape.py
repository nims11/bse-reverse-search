#!/usr/bin/python2
import sys, requests, re, traceback
from lxml import html as xpathDoc
from lxml import etree
import sqlite3
conn = sqlite3.connect('bse.db')

doc = xpathDoc.fromstring(requests.get('http://www.bseindia.com/corporates/Sharehold_Searchnew.aspx').text)
qtrStr = doc.xpath('//td[@class="TTRow"]/a/@href')[0]
qtrid = re.match(r'^.*&qtrid=(.*?)&.*$', qtrStr).group(1)
print("Quarter ID:", qtrid)

cursor = conn.execute('SELECT scrip_id from bhav')
def scrape_patterns(scrip_id):
    cur = conn.cursor()
    url = r'https://www.bseindia.com/corporates/shpPublicShareholder.aspx?scripcd='+str(scrip_id)+r'&qtrid='+qtrid+'&QtrName=March%202015'
    html = requests.get(url).text
    doc = xpathDoc.fromstring(html)
    name = doc.xpath('//td[@class="TTRow"]//text()')[0]
    cur.execute("UPDATE bhav SET name = ? WHERE scrip_id = ?", (name, scrip_id))
    print(scrip_id, name, url, end='\r')
    for rows in doc.xpath('//div[@id="tdData"]/table/tr[3]/td/table/tr')[2:-2]:
        try:
            name = rows.xpath('./td[2]/text()')[0]
            if(len(name) == 0):
                continue
            share_num = int(rows.xpath('./td[4]/text()')[0].strip())
            # print(etree.tostring(rows))
            share_per = float(rows.xpath('./td[8]/text()')[0].strip())
            print("Adding %d shares (%f) for %s (%d)" % (share_num, share_per, name, scrip_id), end='\r')
        except Exception as e:
            continue
            # traceback.print_exc()
        query = """
INSERT OR IGNORE INTO shareholding_patterns
(scrip_id, name, num_share, percent_share, qtr_id) VALUES 
(?, ?, ?, ?, ?)
        """
        cur.execute(query, (scrip_id, name, share_num, share_per, float(qtrid)))
    conn.commit()


for row in cursor:
    try:
        scrip_id = row[0]
        scrape_patterns(scrip_id)
    except KeyboardInterrupt:
        break
    except:
        print('ERROR:', scrip_id)

conn.close()
