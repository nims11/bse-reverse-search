import requests
from lxml import html as xpathDoc
import subprocess
import sqlite3
conn = sqlite3.connect('bse.db')
cursor = conn.cursor()
URL = 'http://www.bseindia.com/markets/equity/EQReports/Equitydebcopy.aspx'

def process(line):
    """ Store bhavs in db
    """
    scrip_id, bhav = int(line[0]), float(line[7])
    cursor.execute('INSERT OR IGNORE INTO bhav (scrip_id, value) VALUES (%s, %f)' % (scrip_id, bhav))
    cursor.execute('UPDATE bhav SET value = %f WHERE scrip_id = %d' % (bhav, scrip_id))


if __name__ == '__main__':
    html = requests.get(URL).text
    doc = xpathDoc.fromstring(html)
    zip_url = doc.xpath('//li[@id="liZip"]/a/@href')[0]
    date = zip_url.split('/')[-1][2:-8]
    date = date[:2] + '/' + date[2:4] + '/' + date[4:]
    cursor.execute('INSERT OR IGNORE INTO stats (key, value) VALUES (?, ?)', ('bhav_date', date))
    cursor.execute("UPDATE stats SET value = ? WHERE key = ?", (date, 'bhav_date'))
    subprocess.call(['wget', zip_url, '-O', 'bhav.zip'])
    csv = subprocess.check_output(['unzip', '-p', 'bhav.zip'])
    for line in csv.splitlines()[1:]:
        process(line.decode('ascii').split(','))

conn.commit()
conn.close()
